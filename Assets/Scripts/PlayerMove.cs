﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float xDelta = Input.GetAxis("Horizontal");
		float yDelta = Input.GetAxis("Vertical");
		if(xDelta != 0 || yDelta != 0){
			Vector3 newPosition = transform.localPosition;
			newPosition.x += xDelta;
			newPosition.y += yDelta;
			transform.localPosition = newPosition;
		}
	}
}
